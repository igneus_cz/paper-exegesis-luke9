**University:** University of South Bohemia in České Budějovice

**Faculty:** Faculty of Theology

**Course:** KTEO/KNENZ Exegeze NZ (New Testament Exegesis)

**Taught by:** Július Pavelčík

**Year:** 2017/18, winter semester

**Author:** Jakub Pavlík

I publish this as an example of a theological paper typeset
in LaTeX.
It is based on my typical paper template, which is available
[elsewhere](https://gist.github.com/igneus/c29bf6bd101348a5ca531c3ca924e20d).

The paper is **non-free for now,** it will be released under one of
content-oriented open source licenses once it's finished
and submitted.

Of particular interest may be examples of Greek New Testament
text, including accents, typeset using the `teubner`
LaTeX package for Greek philology.
